UPenn Timetabler
================

Makes it a bit easier to browse courses at Penn by time. 

Hopefully can be extended to play with data from PennCourseReview. 

---

Info from `http://www.upenn.edu/registrar/timetable/`

Data goes to single .xls, .csv, and .json files.

Disclaimer:
-----------
My job processing the data, especially date/time information, was shoddy at best. 
It's fine for the majority of classes, but do not rely on it!

---

The included excel file should be what you need for now. 
I recommend using the Filter tool to narrow down your course selection. 
For example, I wanted a fun class Monday/Wednesday from 10:30-12. It was pretty easy to find one. 

---

To updated for future semesters: 
`python timetabler.py`

---

Pull requests welcome. 