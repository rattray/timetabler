import tablib, requests, re, time, csv, os
from bs4 import BeautifulSoup
import penncoursereview as pcr
from collections import defaultdict

class TimeTabler:
  '''
  Gets info from Penn Roster and Penn Course Review 
  into more easily machine- and human- readable formats. 
  For more, see docstring at main() and self.data.headers in __init__

  Note that this is, by and large, a quick & dirty implementation 
  that I cooked up to help myself pick classes to audit and feel productive.

  Much thanks to the folks at Penn for making the data available
  and to PennAppsLabs for making the data accessible
  and for providing terrific examples that are the basis for most things here. 
  Note that they do a better job than I ;-) 
  so you should check out github.com/PennAppsLabs

  I think I also took some code/ideas from a script by Nick Meyer. Thanks Nick!

  Author: Alex Rattray (rattray.alex@gmail.com)
  
  '''

  def __init__(self):
    self.start_time = time.time()
    self.dept_codes = ['ACCT','AFST','AFRC','AMCS','ANCH','ANEL','ANTH','ARAB','ARCH','AAMW','ARTH','ASAM','ALAN','ASTR','BENG','BENF','BEPP','BFMD','BCHE','BMB','BE','BIOE','BIOT','BIBB','BIOL','BIOM','BSTA','CAMB','CBE','CHEM','CHIN','CINE','CPLN','CLST','COGS','COLL','COMM','COML','CIS','CIT','CRIM','DEMG','DENT','DTCH','DYNM','EALC','EDCE','EEUR','ECON','EDUC','ESE','EAS','ENM','ENGL','ENVS','EPID','FNCE','FNAR','FOLK','FREN','GAS','GSWS','GCB','GEOL','GRMN','GAFL','GREK','GUJR','HCMG','HPR','HSOC','HEBR','HIND','HSPV','HIST','HSSC','IMUN','INTL','IPD','INTG','INTR','ITAL','JPAN','JWST','KORN','LARP','LATN','LALS','LAW','LGST','LSMP','LING','LGIC','MLYM','MGMT','MKTG','MKSE','MSSP','MTR','MSE','MATH','MEAM','MED','MSCI','MMES','MUSA','MUSC','NSCI','NELC','NGG','NPLD','NURS','OPIM','PERS','PHRM','PHIL','PPE','PHYS','PSCI','PRTG','PSYC','PUBH','PUNJ','REAL','RELS','ROML','RUSS','SKRT','SCND','STSC','SLAV','SWRK','SOCI','SAST','SPAN','STAT','TCOM','TELU','TAML','THAR','TRAN','TURK','URBS','URDU','VLST','WH','WHG','WRIT','YDSH']
    self.data = tablib.Dataset()
    self.data.headers = ['dept', 'code', 'section', 'name', 'instructor', 'type', 'location', 'times', 'days', 'start_time', 'end_time', 'course_quality']
    self.load_api_key()

  def main(self):
    ''' 
    Gets courses from Penn Roster, 
    Attempts to parse all sections
    (what, who, where, when, etc)
    and then find average historical course quality for each course
    and average within-department instructor quality for each course's instructor. 
    Outputs to CSV, XLS, and JSON. 
    '''
    self.courses()
    print 'Done with courses; outputting to CSV'
    self.export_csv()
    print 'output to CSV; about to load instructor data. '
    self.instructors()
    print 'Done with Instructors; outputting to CSV, XLS, and JSON. '
    self.export_csv()
    self.export_xls()
    self.export_json()
    print 'Done outputting to CSV, XLS, and JSON. '

  def courses(self):
    '''
    Saves all information on courses, 
    including course quality, but excluding instructor quality, 
    to self.data 
    '''
    self.courses_dict = {d:defaultdict(list) for d in self.dept_codes}
    for dept_code in self.dept_codes:
      print dept_code
      self.process_dept(dept_code)
    print self.data.csv

  def instructors(self):
    '''
    For each dept, gets all instructor averages.
    Appends instructor quality column to self.data. 
    '''
    self.instructor_averages = {d: self.prof_averages(d) for d in self.dept_codes}
    print self.instructor_averages
    self.data.append_col(self.prof_averages_col, header='instructor_quality')
    print self.data.csv

  def load_csv(self):
    '''
    If you have a half-finished workload 
    (ie; courses ran but instructors did not)
    '''
    with open('courses.csv', 'r') as f:
      reader = csv.reader(f)
      if len(reader.next()) == 13:
        self.data.headers += ['instructor_quality']
      print self.data.headers
      for row in reader:
        self.data.append(row)
    print self.data.csv

  def load_api_key(self):
    '''
    Tries to make sure you have a PCR Auth token in your os environ. 
    If not, attempts to load it from pcr_key.txt. 
    '''
    try:
      print os.environ['PCR_AUTH_TOKEN']
    except:
      print 'no PCR Auth Token detected; looking for one in pcr_key.txt. Save it there if you havent yet. You can get an api key at https://docs.google.com/spreadsheet/viewform?hl=en_US&formkey=dGZOZkJDaVkxdmc5QURUejAteFdBZGc6MQ#gid=0'
      with open('pcr_key.txt', 'r') as f:
        os.environ['PCR_AUTH_TOKEN'] = str(f.read())
        print 'found pcr_key, hopefully. '

  def process_course(self, line):
    '''
    Parses first line in Course block and returns dict of:
    dept, code, name, course_qual. 
    Regex largely thanks to PennAppsLabs's coffeescript roster app. 
    '''
    # CIS -121  PROG LANG AND TECH II             1 CU
    course_pattern = r'^(\w{2,5})\s?\s?-(\d+)\s+(\D*)\s+(\d\.?\d?).*'
    match = re.match(course_pattern, line)
    if not match:
      return None

    parts = match.groups()
    dept = parts[0]
    code = parts[1]
    name = parts[2].title()

    if not code in self.courses_dict[dept].keys():
      course_qual = self.avg_course_quality(dept, code)
      self.courses_dict[dept][code] = course_qual
    else:
      course_qual = self.courses_dict[dept][code]

    course = {
      'dept': dept,
      'code': code,
      'name': name,
      'course_qual': course_qual,
    }
    return course

  def process_section(self, course, line):
    '''
    Parses secondary lines in Course block 
    (many of which are not sections; ignores those.)
    Regex largely thanks to PennAppsLabs's coffeescript roster app. 
    '''

    #  001 LEC TR 10:30-12NOON TOWN 100       TANNEN V
    section_pattern = r'\s*(\d{3})\s+(\w{3})\s+(.*(AM|PM|NOON|TBA))\s+(\w+\s[A-Za-z0-9]+)?\s+(.*)\s*'
    match = re.match(section_pattern, line)
    if not match:
      return None

    parts = match.groups()
    times = parts[2]
    days, start_time, end_time = self.process_times(times)
    location = parts[4]
    name_parts = parts[-1].split()

    section_data = [
      course['dept'],
      course['code'],
      parts[0],  # section
      course['name'],
      parts[-1],  # instructor
      parts[1],  # type
      location,
      times,
      days,
      start_time, 
      end_time,
      course['course_qual'],
    ]
    print time.time() - self.start_time, section_data  # seconds since start, section_data. Nice to know how long it's been chugging, and that it's working =)
    return section_data

  def process_times(self, times):
    '''
    Accepts the rather strange format of
    MW 9-12NOON or whatever and tries to handle it. 
    I don't think it manages well when there are multiple meeting times separated by commas. 
    Ignores AM/PM/NOON. 
    Whenever anything goes wrong, returns ?,?,?
    Tries to return days, start_time, end_time (MW, 9:00, 10:30)
    '''
    try:
      parts = times.split()
      days = parts[0]
      hours = re.sub(r'[A-Z]', '', parts[1]).split('-')
      start = self.process_hour(hours[0])
      end = self.process_hour(hours[1])
      return days, start, end
    except:
      return '?', '?', '?'

  def process_hour(self, hour):
    '''
    Quick and dirty utility to turn eg; 9 to 9:00
    '''
    hour = re.sub(',', '', hour)  # there were commas in there for some reason
    if ':' not in hour:  # ... a bit naive?
      return hour + ':00'
    else:
      return hour

  def process_dept(self, dept_code):
    '''
    Master function to process a department. 
    Gets everything for dept other than instructor code. 
    Saves info to self.data. 
    '''
    r = requests.get('http://www.upenn.edu/registrar/roster/%s.html' % (dept_code.lower()))
    soup = BeautifulSoup(r.text)  # using BeautifulSoup might be overkill but performance shouldn't be too bad on something like this. 
    try:
      text = soup.pre.find_all('p')[-1].string
    except:
      # sometimes pages don't have stuff in them. 
      print '==>>  ignoring dept, think its dead:', dept_code
      return

    # Example Course Block: 

    # CIS -121  PROG LANG AND TECH II             1 CU
    #      REGISTRATION REQUIRED FOR LEC, REC
    #  001 LEC TR 10:30-12NOON TOWN 100       TANNEN V
    #      PERMISSION NEEDED FROM DEPARTMENT
    #      MAX: 140               
    #           RECITATION                        0 CU
    #  201 REC M 11-12NOON TBA                 MEYER N
    #      PERMISSION NEEDED FROM DEPARTMENT
    #      MAX: 20                
    #  202 REC M 12-1PM TBA                      STAFF
    #      PERMISSION NEEDED FROM DEPARTMENT
    #      MAX: 20                
    
    course_blocks = re.split(r'\n\s*\n', text)  
    for course_block in course_blocks:
      lines = course_block.split('\n')
      course = self.process_course(lines[0])
      if course:
        for line in lines[1:]:
          section_data = self.process_section(course, line)
          if section_data:
            self.data.append(section_data)

  def avg_course_quality(self, dept, course):
    '''
    Gets average course qual for given course in dept. 
    Subject to PCR's typical limitations; not doing anything fancy. 
    Might be better to weight by number of reviews submitted?
    '''
    try:
      course_history = pcr.CourseHistory('%s-%s' % (dept, course))
      reviews = course_history.reviews.values
      course_qualities = [float(r.ratings.rCourseQuality) for r in reviews]
      avg = sum(course_qualities) / len(course_qualities)
    except:
      avg = '?'
    return avg

  def is_instructor(self, instructor, first_initial, last_name):
    '''
    In the roster, instructors listed as MINTZ M
    In PCR, instructors listed as ie MAX MINTZ
    This tries, pretty poorly, to see if two names look about equal. 
    '''
    pcr_last = instructor.split()[-1]
    pcr_first = instructor.split()[0]
    if last_name == pcr_last.upper():
      if pcr_first.upper().startswith(first_initial):
        return True
    return False

  def dept_prof_scores(self, dept):
    '''
    For dept, get all instructors
    and then for each build a list of all instructor quality scores
    they have received. Return dict where values are lists. 
    Called from prof_averages
    '''
    try:
      dept = pcr.Department(dept)
      professor_scores = defaultdict(list)
      for review in dept.reviews.values:
        instructor = review.instructor
        rating = review.ratings.rInstructorQuality
        professor_scores[instructor.name].append(float(rating))
      print professor_scores
      return professor_scores
    except Exception, e:
      print '==> OH NOES, COULD NOT GET DEPT', dept, e

  def prof_averages(self, dept):
    '''
    For dept, average all the scores taken down 
    in self.dept_prof_scores (called within). 
    Return dict of profs with avg scores as values. 
    '''
    print dept, time.time() - self.start_time
    print
    print
    professor_scores = self.dept_prof_scores(dept)
    if professor_scores:
      profs = {}
      for prof, scores in professor_scores.iteritems():
        score = sum(scores) / len(scores)
        profs[prof] = score
      return profs
    else:
      return {}

  def prof_averages_col(self, row):
    '''
    Function called by data.append_col
    to get instructor in row, try to find his/her avg score, return it.  
    '''
    instructor_roster = row[4]
    dept = row[0]
    if 'STAFF' in instructor_roster:
      return '?'
    instructor_roster = instructor_roster.split()
    first_initial = instructor_roster[-1] if len(instructor_roster[-1]) == 1 else ''
    last_name = instructor_roster[0]
    for instructor_pcr, score in self.instructor_averages[dept].iteritems():
      if self.is_instructor(instructor_pcr, first_initial, last_name):
        return score

    return '?'

  def export_xls(self):
    with open('courses.xls', 'w') as f:
      # to change format to csv, json, or yaml, just change here. 
      f.write(self.data.xls)

  def export_csv(self):
    with open('courses.csv', 'w') as f:
      f.write(self.data.csv)

  def export_json(self):
    with open('courses.json', 'w') as f:
      f.write(self.data.json)

if __name__ == '__main__':
  tt = TimeTabler()
  tt.load_csv()
  tt.export_xls()
  tt.export_json()
